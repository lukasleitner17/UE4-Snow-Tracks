# UE4 Snow Tracks

UE4 Landscape material which uses runtime virtual texture for displacement to show steps in the snow and sand.

[![Video](https://img.youtube.com/vi/pQsjgl8lYRE/0.jpg)](https://www.youtube.com/watch?v=pQsjgl8lYRE "Video")

![Screenshot1](./Images/Screenshot1.png)
![Screenshot2](./Images/Screenshot2.png)
![Screenshot3](./Images/Screenshot3.png)